# **PROGRAMACION ORIENTADO A OBJETOS**

## _Conceptos_
# **1. Paradigma**
> Un paradigma es todo aquel modelo, patrón o ejemplo que debe seguirse en determinada situación.

> En un sentido amplio, un paradigma es una teoría o conjunto de teorías que sirve de modelo a seguir para resolver problemas.
# **2. Paradigma de programación**
> Los paradigmas de programación hacen referencia a las diferentes formas en las que se puede desarrollar un software y, al mismo tiempo, los diversos enfoques sistemáticos que pueden ser aplicados en todos los niveles del diseño de programas, teniendo como finalidad la resolución de problemas relacionados con lo computacional.
# **3. Definición de la orientación a objetos**
> La orientación a objetos es una técnica de modelado de software a gran escala que sugiere que tratemos la complejidad del sistema software de manera análoga a como los seres humanos tratamos la complejidad en el mundo real: descomponemos el problema en pequeñas piezas con comportamiento propio que nos sirven para componer una solución al problema.

> La orientación a objetos centra su atención en el problema, en lugar de en la solución.

> La orientación a objetos se basa en la estructuración de un sistema en base a sus objetos, en lugar de las acciones que es capaz de realizar.
# **4. Beneficios de la programación orientada a objetos**
> El uso de este paradigma de programación favorece en el desarrollo de software a gran escala y de calidad.

>  Esta metodología de programación favorece en el desarrollo de software, porque permite:
* **Reutilizar código**.
* **Convertir cosas complejas en estructuras simples reproducibles**.
* **Evitar la duplicidad de código**.
* **Trabajar en equipo** gracias al encapsulamiento ya que minimiza la posibilidad de duplicar funciones cuando varias personas trabajan sobre un mismo objeto al mismo tiempo.
* **Corregir errores** en varios lugares del código.
* **Proteger la información** a través de la encapsulación, ya que solo se puede acceder a los datos del objeto a través de propiedades y métodos privados.
* **Reducir la complejidad** atravéz de la abstracción, ya que permite construir sistemas más complejos de forma más sencilla y organizada.

**NOTA:** La abstracción es un concepto que usamos de forma natural para describir los objetos que nos rodean. Consiste en definir un objeto a partir de sus características más representativas (atributos y métodos), permitiendo describir de manera simple un objeto mucho más complejo. Por ejemplo, si pensamos en un automóvil, naturalmente pensamos en un vehículo de cuatro ruedas y un timón (atributos) que se desplaza hacia adelante o hacia atrás a diferentes velocidades (métodos).
# **5. Objeto**
> Un objeto en programación orientada a objetos ***representa alguna entidad de la vida real***, es decir, alguno de los objetos únicos que pertenecen al problema con el que nos enfrentamos y con el que podemos interactuar.

> **Cada objeto**, de igual modo que la entidad de la vida real a la que representa, **tiene un estado** (es decir, unos ==atributos== con unos valores concretos) **y un comportamiento** (es decir, tiene ==funcionalidades== o sabe hacer unas acciones concretas).

> Un objeto representa un concepto de negocio, una pieza de abstracción del software cuyo objetivo es resolver un problema único y concreto, manteniendo la simplicidad y la manipulación intuitiva.

> **NOTA:** Los objetos del **mundo real** se denominan **entidades** y en el **mundo de la programación orientada a objetos** se denominan **objeto**
# **6. Clase**
> Una clase es un elemento de la programación orientada a objetos que actúa como una plantilla y va a definir las características y comportamientos de una entidad. La clase va a ser como un molde a partir del cual vamos a poder definir entidades.

> Una Clase es el modelo por el cual nuestros objetos se van a construir y nos van a permitir generar más objetos.

> Una clase es la definición de un objeto que incluye:
* Nombre
* Atributos, que son los datos que definen su estado.
* Métodos, que son las funciones que implementa y que definen su comportamiento.
**Representación gráfica de un objeto**

  ![clase](https://2.bp.blogspot.com/-GNHmDKzeqcI/WJj8EFzPbxI/AAAAAAAABZ8/iH9R4gmpdMUsYaOGLU3TYW_oB9nTyECXwCK4B/s1600/elementos.PNG)
> **Elementos de una clase**
Una clase está formada por variables y funciones. Estrictamente hablando en términos de programacion orientada a objetos, las variables se denominan atributos y a las funciones se denominan métodos. A este binomio formado por los atributos y los métodos se denomina miembros de una clase.
* **Atributos**
Los atributos, también llamados campos, son variables que codifican el estado de un objeto

  **Ejemplo:** Si tenemos la clase Persona con los atributos nombre y edad –de tipo cadena de caracteres y entero, respectivamente–, cada objeto que se defina del tipo Persona tendrá estos dos atributos.
El estado de cada objeto Persona dependerá de los valores que se les asigne a estos dos atributos, tal como se ve en la figura siguiente.

  ![ejemplo](https://i.ibb.co/cwR6JKV/81514-m2-001.gif)
* **Métodos**
Los métodos implementan el comportamiento de un objeto o, dicho de otro modo, las funcionalidades que un objeto es capaz de realizar.
 Haciendo una analogía con la programación estructurada, los métodos serían como funciones (devuelvan algo o no). De ahí que un método, además de por el nombre, se caracteriza por los argumentos (o también llamados parámetros) de entrada que recibe y por el valor de retorno que resulta de ejecutar el comportamiento que implementa.
  * Sobrecarga de métodos, la sobrecarga de métodos es la creación de varios métodos con el mismo nombre, pero con diferente lista de tipos de parámetros.
* **Constructor y destructor**
Las clases tienen dos tipos de métodos especiales llamados constructor y destructor que no se consideran miembros de una clase, como tales. No son miembros de la clase porque ni el constructor ni el destructor se heredan.
La mayoría de los lenguajes de programación orientados a objetos implementan el método constructor, incluso algunos obligan a codificar explícitamente uno. No ocurre lo mismo con el destructor, cuya codificación se puede obviar en muchos lenguajes.
  * **Constructor**
El constructor se llama de forma automática cuando se crea un objeto para situarlo en memoria e inicializar los atributos declarados en la clase. En la mayoría de lenguajes, el constructor tiene las siguientes características:
  1) Normalmente, el nombre del constructor es el mismo que el de la clase.
  2) El constructor no tiene tipo de retorno, ni siquiera void.
  3) Puede recibir parámetros (o también llamados argumentos) con el fin de inicializar los atributos de la clase para el objeto que se está creando en ese momento.
  4) En general suele ser público, pero algunos lenguajes permiten que sea privado.
  
  **NOTA:**  Hay lenguajes que permiten crear más de un constructor, por ejemplo C++, C# y Java, entre otros. En estos casos, al constructor sin parámetros/argumentos se le suele llamar constructor por defecto o predeterminado, mientras que aquellos que tienen parámetros se les llama constructores con argumentos. Como se puede apreciar, decir «constructor por defecto» y «con argumentos» es lo mismo que decir que se hace una sobrecarga del constructor. Debido a la sobrecarga, la única limitación cuando se quiere (y se puede) crear más de un constructor es que no pueden declararse varios constructores con el mismo número y el mismo tipo de argumentos.
  En los lenguajes en los que solo se puede codificar un constructor –por ejemplo, PHP5 y Python–, a este se le llama simplemente constructor.
  En muchos lenguajes, como por ejemplo Java o C#, si no se define ningún constructor para la clase, el propio compilador creará un constructor por defecto –es decir, sin argumentos– que no hará nada especial más allá de ubicar el objeto en memoria. En el momento en que el programador implementa un constructor, el compilador no añadirá automáticamente el constructor por defecto, aunque el constructor implementado por el programador sea con argumentos.
  Por último, hay que resaltar que, en muchos lenguajes, no es obligatorio que una clase tenga un constructor por defecto. Puede interesarnos que todos sus constructores sean con argumentos.
  * **Destructor**
  El destructor es el método que sirve para eliminar un objeto concreto definitivamente de memoria. Hay que tener en cuenta que:
  1) No todos los lenguajes necesitan implementar un método destructor.
  2) Por norma general, una clase tiene un solo destructor.
  3) En algunos lenguajes no tiene tipo de retorno, ni siquiera void. En otros, generalmente, tiene void como tipo de retorno.
  4) No recibe parámetros.
  5) En general suele ser público.

    **NOTA:** La manera en la que se declara el método destructor varía entre lenguajes. Por ejemplo, en C++ y C#, el nombre del destructor es el mismo que el de la clase precedido por el símbolo ~, por ejemplo ~Persona().
    En Java, en cambio, se usa el método especial finalize() que no devuelve nada (en este caso, sí tiene tipo de retorno, concretamente void). El compilador de Java no obliga a implementar el método finalize(). Así pues, solo se debe codificar si realmente es necesario.
# **7. Abstracción y encapsulamiento**
* **Abstracción**
* Es una descripción de especificación que enfatiza algunos de los detalles o propiedades de algo.
* Consiste en captar las características esenciales de un objeto, así como su comportamiento.
* Es una forma por el cual obtenemos información de una entidad, los cuales son sus características y funciones que desempeñan.

  **Ejemplos de abstracción**
  
  **Ejemplo 1:** ¿Qué características podemos abstraer de los automóviles? o ¿Qué características semejantes tienen todos los automóviles?
  Características: Marca, Modelo, Número de chasis, Peso llantas o cauchos, Puertas, Ventanas... Comportamiento: Acelerar, Frenar, Retroceder...
  
  **Ejemplo 2:** La gerencia de un taller mecánico necesita un sistema para controlar los vehículos que ingresan a sus instalaciones. En este caso, las características esenciales de la clase vehículo son: Marca, Modelo, Color, Falla detectada, Nombre del Propietario, Dirección del Propietario, Teléfono del Propietario...
  
  **NOTA:** A esto accion se llama abstracción. En general un programa no es más que una descripción abstracta de un procedimiento o fenómeno que existe o sucede en el mundo real.
  * La abstracción es crucial para comprender este complejo mundo.
  * La abstracción es esencial para el funcionamiento de una mente humana normal y es una herramienta muy potente para tratar la complejidad.
  * La abstracción es clave para diseñar un buen software.

* **Encapsulamiento**
Consiste en unir en la Clase las características y comportamientos, esto es, las variables y métodos. Es tener todo esto en una sola entidad.
En los lenguajes estructurados esto era imposible. Es evidente que el encapsulamiento se logra gracias a la abstracción. La utilidad del encapsulamiento va por la facilidad para manejar la complejidad, ya que tendremos a las Clases como cajas rojas donde sólo se conoce el comportamiento, pero no los detalles internos, y esto es conveniente porque nos interesará conocer que hace la Clase pero no será necesario saber cómo lo hace.

  **Ejemplos de encapsulamiento**
  
  **Ejemplo 1:** De un televisor, el usuario conoce su apariencia y parte de su funcionamiento. Sólo le importa que funcionen el selector de canales, el video y el audio; no le interesa saber cómo funciona cada una de las partes internas del aparato, esos detalles sólo le interesan al fabricante y al técnico de servicio.
  
  **Ejemplo 2:** De un animal no sólo es necesario conocer su apariencia; también se requiere conocer qué sabe hacer y cómo reacciona ante determinadas situaciones.
  
  **NOTA:** Haciendo una analogía, ***el encapsulamiento*** se describiría como ***una caja negra***, porque Un objeto en el que su comportamiento y atributos son conocidos pero no su trabajo interno, el cual continua siendo un misterio.
